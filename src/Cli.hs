{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

-- | CLI, input and output parsing.
module Cli where

import App (AppErr (..), GiftConfig (GiftConfig, outPathPrefix), GiftCycle)
import Data.Aeson (eitherDecodeFileStrict)
import Data.Text.IO qualified as TIO
import Options.Applicative
import RIO.Text (Text, unpack)
import System.Directory qualified as Directory

-- | CLI options.
newtype Args = Args
  { -- | Config file corresponding to 'GiftConfig' JSON.
    file :: FilePath
  }

configFile :: Parser FilePath
configFile = strOption (long "file" <> short 'f' <> help "Config file path as JSON")

args :: Parser Args
args = Args <$> configFile

cliArgs :: ParserInfo Args
cliArgs = info (args <**> helper) (header "Uniform gift exchange selection")

-- | Read the json config.
parseConfig :: Args -> IO (Either AppErr GiftConfig)
parseConfig = fmap (either (Left . ConfigParseErr) Right) . eitherDecodeFileStrict . file

giftMoji :: Text
giftMoji = "🎁"

-- | Construct the file text.
giftMessage :: (Text, Text) -> Text
giftMessage (gifter, giftee) =
  giftMoji
    <> giftMoji
    <> " "
    <> gifter
    <> ", your gift is for "
    <> giftee
    <> " "
    <> giftMoji
    <> giftMoji

-- | Write gift message to a file.
writeGiftMessage :: GiftConfig -> (Text, Text) -> IO ()
writeGiftMessage GiftConfig {outPathPrefix} m@(gifter, _) = do
  absDir <- Directory.makeAbsolute (unpack outPathPrefix)
  Directory.createDirectoryIfMissing True absDir
  let file = absDir <> "/" <> unpack gifter <> "_gift.txt"
      msg = giftMessage m

  TIO.writeFile file msg

writeAllGiftMessages :: GiftConfig -> GiftCycle -> IO ()
writeAllGiftMessages config = mapM_ (writeGiftMessage config)
