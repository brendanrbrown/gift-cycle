{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StrictData #-}

-- |
-- TODO:
module App where

import Control.Monad (unless, (>=>))
import Control.Monad.ST (runST)
import Data.Aeson (FromJSON, ToJSON)
import Data.Bifunctor (bimap)
import qualified Data.IntMap as IM
import Data.STRef
import qualified Data.Vector.Algorithms.Merge as VA
import RIO
import qualified RIO.Map as M
import qualified RIO.Set as S
import qualified RIO.Vector as V
import RIO.Vector.Partial ((!))
import RandomCycle.Vector (uniformCyclePartitionThin)
import System.Random.MWC (GenIO, createSystemSeed, fromSeed, initialize)
import System.Random.MWC.Distributions (uniformPermutation, uniformShuffleM)

{- APP -}

-- TODO: just use mtl to use a proper error monad
type GiftApp = RIO GiftConfig

{- CONFIG -}

-- TODO: switch for rule that disallows
-- cycles of length two? (two people givig to each other)

-- | TODO: specify json shape reqd. Input shape for matching rules.
-- note that "no self gifts" rule is imposed always.
-- @participants@ gives a full list of participants.
data RuleConfig = RuleConfig
  { include :: S.Set (Text, Text),
    exclude :: S.Set (Text, Text),
    -- | Are cycles of length two allowed? As in, can a person receive
    -- a gift from the person they give to?
    noTwoCycle :: Bool
  }
  deriving stock (Show, Generic)
  deriving anyclass (FromJSON, ToJSON)

-- | Full input shape for @GiftApp@ configuration.
data GiftConfig = GiftConfig
  { rule :: RuleConfig,
    -- | Vector of participants. Duplicate entries will generate
    -- an 'AppError'.
    participants :: Vector Text,
    -- | Output file to <outPathPrefix>/<participant>_santa.txt.
    outPathPrefix :: Text,
    -- | Seed for random-number generator.
    -- Otherwise will use system entropy.
    seed :: Maybe Word32,
    -- | Max number of iterations for sampler.
    maxit :: Maybe Natural,
    -- | Max number of times you can fail 'noTwoCycle' check,
    -- if applicable.
    maxitNoTwoCycle :: Maybe Natural
  }
  deriving stock (Show, Generic)
  deriving anyclass (FromJSON, ToJSON)

{- Validation -}

-- | The participants list is valid if its entries are unique.
-- This index will be used to remap participants to their index-based pairings.
newtype ValidatedParticipants = ValidatedParticipants {getParticipants :: Vector Text}
  deriving stock (Show, Generic)
  deriving newtype (FromJSON, ToJSON)

validatedParticipants :: Vector Text -> Either AppErr ValidatedParticipants
validatedParticipants v =
  if n == V.length v
    then Right (ValidatedParticipants v)
    else Left InvalidParticipants
  where
    n = V.length $ runST $ (V.thaw >=> VA.sortUniq >=> V.freeze) v

-- TODO: in some cases, it is likely to be better to recast the exclusion rules
-- as a single set of allowable edges. that is an expensive operation,
-- so there will be some cutoff in number of exclusions making it better
-- to build the map of allowable edges (querying a small map for existence)
-- rather than to query a large map of excluded edges every check of the gift rules.

-- | Transform edges labeled by text to those labeled by Int using Map.
edgesToInts :: AppErr -> S.Set (Text, Text) -> M.Map Text Int -> Either AppErr [(Int, Int)]
edgesToInts err es nm = foldl' op (Right []) es
  where
    op (Left e) _ = Left e
    op res x = case lookPair x of
      Nothing -> Left err
      Just p -> (p :) <$> res
    lookPair :: (Text, Text) -> Maybe (Int, Int)
    lookPair (gifter, giftee) = (,) <$> M.lookup gifter nm <*> M.lookup giftee nm

-- | Name -> Id, for fast lookup by name rather than id.
type NameToId = M.Map Text Int

nameToId :: ValidatedParticipants -> NameToId
nameToId = M.fromList . map (\(x, y) -> (y, x)) . V.toList . V.indexed . getParticipants

-- | Edges key -> value must be included.
newtype ValidatedInclRuleInt = ValidatedInclRuleInt {getInclRuleInt :: S.Set (Int, Int)}
  deriving newtype (Show)

inclRuleInt :: RuleConfig -> NameToId -> Either AppErr ValidatedInclRuleInt
inclRuleInt RuleConfig {..} = fmap (ValidatedInclRuleInt . S.fromList) . edgesToInts InvalidInclRule include

-- | Edges key -> value cannot be included.
newtype ValidatedExclRuleInt = ValidatedExclRuleInt {getExclRuleInt :: S.Set (Int, Int)}
  deriving newtype (Show)

exclRuleInt :: RuleConfig -> NameToId -> Either AppErr ValidatedExclRuleInt
exclRuleInt RuleConfig {..} = fmap (ValidatedExclRuleInt . S.fromList) . edgesToInts InvalidExclRule exclude

{- INTERNAL REP AND UTILITIES -}

---- | Convert the inclusion rules to a list of exclusion predicates.
---- Each predicate must return 'True' if the edge must be *excluded*.
inclRuleToExclPredicate :: ValidatedInclRuleInt -> [(Int, Int) -> Bool]
inclRuleToExclPredicate = foldl' op [] . getInclRuleInt
  where
    op ps x = chk x : ps
    chk (gifter, giftee) (gr, ge)
      | gr == gifter = ge /= giftee
      | ge == giftee = gr /= gifter
      | otherwise = False

-- | Transform 'RuleConfig' into a set of rules on edges of type (Int, Int). It checks InclRule first,
-- anticipating this to be the smaller of the two. Returns 'True' if the edge satisfies the rule, and
-- if it is not a self loop.
giftRuleExcl :: ValidatedInclRuleInt -> ValidatedExclRuleInt -> (Int, Int) -> Bool
giftRuleExcl validIm (ValidatedExclRuleInt em) e = uncurry (/=) e && chkExcl e && chkExclFromIncl e
  where
    chkExcl e' = S.notMember e' em
    chkExclFromIncl e' = not $ any ($ e') $ inclRuleToExclPredicate validIm

-- | If 'noCycleLengthTwo', check that condition on the integer representation.
hasCycleLengthTwo :: Vector (Int, Int) -> Bool
hasCycleLengthTwo v = any chk v
  where
    chk x = any (\(gifter, giftee) -> (giftee, gifter) == x) v

-- | If noTwoCycle is False (meaning it should not be checked), chkTwoCycle should be const False.
uniformCyclePartitionThinWrapper :: Int -> (Vector (Int, Int) -> Bool) -> Int -> ((Int, Int) -> Bool) -> Int -> GenIO -> GiftApp (Either AppErr (Vector (Int, Int)))
uniformCyclePartitionThinWrapper itTwoCycle chkTwoCycle it chk n g = do
  if itTwoCycle <= 0
    then pure $ Left NoMatchFound
    else do
      res <- uniformCyclePartitionThin it chk n g

      case res of
        Nothing -> pure $ Left NoMatchFound
        Just ms -> do
          -- If there is a two-cycle, rerun and decrement counter.
          if chkTwoCycle ms
            then uniformCyclePartitionThinWrapper (itTwoCycle - 1) chkTwoCycle it chk n g
            else pure $ Right ms

-- uniformCyclePartitionThin (fromIntegral it) (giftRuleExcl im em) n g

{- OUTPUT -}

-- | Result split between the must-include edges and the ones to be matched.

-- | A gift pairing is a direct cycle partition graph
-- of the participant labels, with a -> b meaning a gives a gift to b.
type GiftCycle = Vector (Text, Text)

-- | TODO: it is the caller's responsibility to ensure the inputs have the
-- same length and that the (Int, Int) values are in range.
intCycleToGiftCycle :: Vector Text -> Vector (Int, Int) -> GiftCycle
intCycleToGiftCycle ps = V.map (bimap (ps !) (ps !))

{- EXCEPTIONS -}

data AppErr
  = InvalidParticipants
  | NoMatchFound
  | InvalidInclRule
  | InvalidExclRule
  | ConfigParseErr String
  | FailedFinalCheck
  deriving stock (Show, Eq, Generic)
  deriving anyclass (Exception)

{- MATCH -}

maxitDefault :: Natural
maxitDefault = 10000

-- | Max number of times you can fail on noTwoCycle, if applicable.
maxCheckNoTwoCycleDefault :: Natural
maxCheckNoTwoCycleDefault = 1000

-- | Main work. Match n participants (labeled 0..n-1) subject to the rules, which are validated here.
validateAndMatchInt :: GiftApp (Either AppErr (Vector (Int, Int)))
validateAndMatchInt = do
  GiftConfig {..} <- ask
  s <- liftIO $ case seed of
    Nothing -> fromSeed <$> createSystemSeed
    Just s' -> pure $ V.singleton s'
  g <- liftIO $ initialize s
  ps <- either throwIO pure $ validatedParticipants participants

  let it = fromMaybe maxitDefault maxit
      itTwoCycle = fromMaybe maxCheckNoTwoCycleDefault maxitNoTwoCycle
      nm = nameToId ps
      n = V.length participants

  -- TODO: see note about using mtl to use monaderror to clean this up
  (im, em) <- either throwIO pure $ (,) <$> inclRuleInt rule nm <*> exclRuleInt rule nm

  uniformCyclePartitionThinWrapper
    (fromIntegral itTwoCycle)
    hasCycleLengthTwo
    (fromIntegral it)
    (giftRuleExcl im em)
    n
    g

match :: GiftApp GiftCycle
match = do
  ps <- asks participants
  iMatch <- validateAndMatchInt
  either throwIO (pure . intCycleToGiftCycle ps) iMatch
