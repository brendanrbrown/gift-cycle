{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import App
import Cli (cliArgs, parseConfig, writeAllGiftMessages)
import Control.Exception (throwIO)
import Options.Applicative (execParser)
import RIO (runRIO)
import RIO.Set qualified as S
import RIO.Vector qualified as V

main :: IO ()
main = do
  args <- execParser cliArgs

  config <- either throwIO pure =<< parseConfig args

  res <- runRIO config match

  writeAllGiftMessages config res
  putStrLn "All participants matched!"
